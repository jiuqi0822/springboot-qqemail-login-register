1.  # springboot-QQEmail-Login-Register

    #### 介绍

    **基于springboot的QQ邮箱登录和注册**

    #### 环境要求

    - **JDK8**
    - **MySQL8**
    - **springboot版本号2.4.5(使用的当前最新版)**

    #### 基本模块演示

    1. **登录页面**

       > 用户有三种状态
       >
       > 1. 数据库中存在用户
       >    - status: 1 已激活可登录 字体为绿色
       >    - status:2 未激活不可登录 字体为红色
       >
       > 2. 数据库中不存在用户 字体为黑色

       ![image-20210418131058047](https://gitee.com/jiuqi0822/images/raw/master/20210418131059.png)

    2. **注册页面**

       > 注册时输入完显示当前用户是否重复可用(不显示默认不重复可用)

       ![image-20210418131409241](https://gitee.com/jiuqi0822/images/raw/master/20210418131411.png)

    3. 发送邮箱，由于没有搭载服务器，点击邮箱链接只能在运行本项目的电脑上操作

    #### 运行项目

    1. 运行sql语句`在src/main/resources/sql/qqemail.sql`
    2. 修改application.properties中的一些个人配置
    3. 本项目默认的访问地址是`localhost:8091`

    
