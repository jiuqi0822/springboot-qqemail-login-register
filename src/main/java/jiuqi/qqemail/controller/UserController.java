package jiuqi.qqemail.controller;

import jiuqi.qqemail.common.UUIDUtils;
import jiuqi.qqemail.entity.User;
import jiuqi.qqemail.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 注册
     *
     * @param user
     * @return
     */
    @RequestMapping("/registerUser")
    public String register(User user, Model model) {
        if (userService.exitUsername(user)) {
            model.addAttribute("msg", "用户名存在，请重新注册");
            return "lr/register";
        } else {
            user.setStatus(0);
            String code = UUIDUtils.getUUID() + UUIDUtils.getUUID();
            user.setCode(code);
            userService.register(user);
            return "success";
        }
    }

    @ResponseBody
    @RequestMapping(value = "/verityUser/{username}", method = RequestMethod.GET)
    public String verityUser(@PathVariable("username") String username) {
        return userService.verifyUser(username);
    }

    @ResponseBody
    @RequestMapping(value = "/usernameStatus/{username}", method = RequestMethod.GET)
    public int usernameStatus(@PathVariable("username") String username, Model model) {
        return userService.usernameStatus(username);
    }

    /**
     * 校验邮箱中的code激活账户
     * 首先根据激活码code查询用户，之后再把状态修改为"1"
     */
    @RequestMapping("/checkCode")
    public String checkCode(String code) {
        User user = userService.checkCode(code);
        System.out.println(user);
        //如果用户不等于null，把用户状态修改status=1
        if (user != null) {
            //已经点击邮箱链接 将状态改为1
            user.setStatus(1);
            System.out.println("用户=>" + user.getUsername() + "的code=>" + user.getCode());
            //把code验证码清空，已经不需要了
            user.setCode("");
            System.out.println(user);
            userService.updateUserStatus(user);
        }
        return "lr/login";
    }

    /**
     * 跳转到注册页面
     *
     * @return login
     */
    @RequestMapping("/toRegisterPage")
    public String index() {
        return "lr/register";
    }

    /**
     * 跳转到登录页面
     *
     * @return login
     */
    @RequestMapping("/toLoginPage")
    public String login() {
        return "lr/login";
    }


    /**
     * 登录
     */
    @RequestMapping("/loginUser")
    public String login(User user, Model model) {
        User u = userService.loginUser(user);
        if (u != null) {
            //登录成功  跳转到welcome欢迎页
            return "welcome";
        } else {
            model.addAttribute("msg", "用户名或密码有误，请确认是否已激活!");
            return "lr/login";
        }
    }

    @RequestMapping("/toUpdatePasswordPage")
    public String toUpdatePasswordPage() {
        return "othersPage/verifyUsernameUseremailPage";
    }

    @RequestMapping("/exitToUpdate")
    public String exitToUpdate(User user,Model model) {
        User u = userService.exitUsernameAndEmail(user);
        System.out.println("填写的用户信息user===>"+user);
        System.out.println("从数据库中返回的用户信息user===>"+u);
        if (u != null) {
            user.getUsername();
            model.addAttribute("msg", user.getUsername()+"用户存在，请点击链接确认即可登录！");
            user.setStatus(0);
            String code = UUIDUtils.getUUID() + UUIDUtils.getUUID();
            user.setCode(code);
            //更改status=0和code赋值
            userService.updateStatusCode(user);
            //发送邮箱
            userService.sendUpdateEmail(user);
            //修改密码 但此时的status=0  不点击链接不可登录
            userService.updatePassword(user);
            return "lr/login";
        } else {
            model.addAttribute("msg", "用户或邮箱不存在，不可修改");
            return "othersPage/verifyUsernameUseremailPage";
        }
    }

    @RequestMapping("/checkCodeAndUpdateStatusCodeIsNull")
    public String checkCodeAndUpdateStatusCodeIsNull(String code) {
        User user = userService.checkCode(code);
        System.out.println(user);
        //如果用户不等于null，把用户状态修改status=1
        if (user != null) {
            //已经点击邮箱链接 将状态改为1
            user.setStatus(1);
            System.out.println("用户=>" + user.getUsername() + "的code=>" + user.getCode());
            //把code验证码清空，已经不需要了
            user.setCode("");
            System.out.println(user);
            userService.updateUserStatus(user);
        }
        return "lr/login";
    }

}
