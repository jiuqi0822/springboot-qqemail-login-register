package jiuqi.qqemail.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    /**
     * 首页
     */
    @RequestMapping({"/","/index"})
    public String index() {
        return "lr/login";
    }
}
