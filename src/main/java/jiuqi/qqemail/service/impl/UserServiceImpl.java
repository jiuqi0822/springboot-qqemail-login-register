package jiuqi.qqemail.service.impl;


import jiuqi.qqemail.dao.UserDao;
import jiuqi.qqemail.entity.User;
import jiuqi.qqemail.service.EmailService;
import jiuqi.qqemail.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    /**
     * 注入邮件接口
     */
    @Autowired
    private EmailService mailService;

    /**
     * 用户注册，同时发送一封激活邮件
     *
     * @param user
     */
    @Override
    public void register(User user) {
        userDao.register(user);

        //获取激活码
        String code = user.getCode();
        System.out.println("code:" + code);
        //主题
        String subject = "这是贾浩浩给您发的注册时激活用户认证的邮件(如未执行操作，忽略此邮件)";
        //方式1
        String context = "<a href=\"http://localhost:8091/user/checkCode?code=" + code + "\">激活用户认证请点击=> " + code + "</a>";
        //发送激活邮件
        mailService.sendHtmlMail(user.getUseremail(), subject, context);
    }

    /**
     * 根据激活码code进行查询用户，之后再进行修改状态
     *
     * @param code
     * @return
     */
    @Override
    public User checkCode(String code) {

        return userDao.checkCode(code);
    }

    /**
     * 激活账户，修改用户状态
     *
     * @param user
     */
    @Override
    public void updateUserStatus(User user) {
        userDao.updateUserStatus(user);
    }

    /**
     * 登录
     *
     * @param user
     * @return
     */
    @Override
    public User loginUser(User user) {
        User user1 = userDao.loginUser(user);
        if (user1 != null) {
            return user1;
        }
        return null;
    }

    @Override
    public int usernameStatus(String status) {
        return userDao.usernameStatus(status) == 1 ? 1 : 0;
    }

    /**
     * 判断用户是否存在
     *
     * @param user
     * @return
     */
    @Override
    public boolean exitUsername(User user) {
        return userDao.exitUsername(user) == null ? false : true;
    }

    /**
     * ajax判断用户名是否已存在
     *
     * @param username
     * @return
     */
    @Override
    public String verifyUser(String username) {
        return userDao.verifyUser(username) == null ? "SUCCESS" : "ERROR";
    }

    /**
     * 检查username和usereamil是否存在
     * @param user
     * @return
     */
    @Override
    public User exitUsernameAndEmail(User user) {
        return userDao.exitUsernameAndEmail(user);
    }

    /**
     * 检查username和usereamil是否存在
     * @param user
     * @return
     */
    @Override
    public void sendUpdateEmail(User user) {
        //获取激活码
        String code = user.getCode();
        System.out.println("code: " + code);
        //主题
        String subject = "这是贾浩浩给您发的修改用户密码的邮件(如未执行操作，忽略此邮件)";
        //方式1
        String context = "<a href=\"http://localhost:8091/user/checkCodeAndUpdateStatusCodeIsNull?code=" + code + "\">激活用户认证请点击=> " + code + "</a>";
        //发送激活邮件
        mailService.sendHtmlMail(user.getUseremail(), subject, context);
    }

    /**
     * 发送邮箱链接时把用户的status改为0和code的值
     * @param user
     */
    @Override
    public void updateStatusCode(User user) {
        userDao.updateStatusCode(user);
    }

    /**
     * 更改密码
     * @param user
     * @return
     */
    @Override
    public int updatePassword(User user) {
        return userDao.updatePassword(user);
    }
}
