package jiuqi.qqemail.service;


import jiuqi.qqemail.entity.User;

public interface UserService {
    /**
     * 用户注册
     *
     * @param user
     */
    void register(User user);

    /**
     * 根据激活码code查询用户，之后再进行修改状态
     *
     * @param code
     * @return
     */
    User checkCode(String code);

    /**
     * 激活账户，修改用户状态
     *
     * @param user
     */
    void updateUserStatus(User user);

    /**
     * 登录，根据用户状态为“1”来查询
     *
     * @param user
     * @return
     */
    User loginUser(User user);

    /**
     * 查出当前用户的状态
     */
    int usernameStatus(String status);

    /**
     * 执行完后判断当前用户是否可用
     * 重复性
     * @param user
     * @return
     */
    public boolean exitUsername(User user);

    /**
     * ajax判断用户名是否已存在
     * @param username
     * @return
     */
    String verifyUser(String username);

    /**
     * 检查username和usereamil是否存在
     * @param user
     * @return
     */
    User exitUsernameAndEmail(User user);

    /**
     * 检查username和usereamil是否存在
     * @param user
     * @return
     */
    void sendUpdateEmail(User user);

    /**
     * 发送邮箱链接时把用户的status改为0和code的值
     * @param user
     */
    void updateStatusCode(User user);

    /**
     * 更改密码
     * @param user
     * @return
     */
    int updatePassword(User user);

}

