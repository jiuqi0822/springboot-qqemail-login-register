package jiuqi.qqemail.dao;

import jiuqi.qqemail.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface UserDao {
    /**
     * 用户注册，注册的时候默认状态为0：未激活，并且调用邮件服务发送激活码到邮箱
     *
     * @param user
     */
    @Insert("insert into useremail ( username, password,useremail,status,code) values (#{username,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, #{useremail,jdbcType=VARCHAR}, #{status,jdbcType=INTEGER},#{code,jdbcType=INTEGER})")
    void register(User user);

    /**
     * 点击邮箱中的激活码进行激活，根据激活码查询用户，之后再进行修改用户状态为1进行激活
     *
     * @param code
     * @return
     */
    @Select("select * from useremail where code = #{code}")
    User checkCode(String code);

    /**
     * 激活账户，修改用户状态为“1”进行激活
     *
     * @param user
     */
    @Update("update useremail set status=1,code=null where id=#{id}")
    void updateUserStatus(User user);

    /**
     * 登录，根据用户状态为“1”来查询
     *
     * @param user
     * @return
     */
    @Select("select * from useremail where username=#{username} and password=#{password} and status=1")
    User loginUser(User user);

    /**
     * 查出当前用户的状态
     */
    @Select("select status from useremail where username=#{username}")
    int usernameStatus(String status);

    /**
     * 执行完后判断当前用户是否可用
     * 重复性
     * @param user
     * @return
     */
    @Select("select * from useremail where username=#{username}")
    User exitUsername(User user);

    /**
     * ajax判断用户名是否已存在
     * @param username
     * @return
     */
    @Select("select * from useremail where username=#{username}")
    User verifyUser(String username);

    /**
     * 检查username和usereamil是否存在
     * @param user
     * @return
     */
    @Select("select * from useremail where username=#{username} and useremail=#{useremail}")
    User exitUsernameAndEmail(User user);

    /**
     * 发送邮箱链接时把用户的status改为0和code的值
     * @param user
     */
    @Update("update useremail set status=#{status}, code=#{code} where username=#{username}")
    void updateStatusCode(User user);

    /**
     * 更改密码
     * @param user
     * @return
     */
    @Update("update useremail set password=#{password} where username=#{username}")
    int updatePassword(User user);

}

