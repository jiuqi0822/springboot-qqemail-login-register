/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : qqemail

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 18/04/2021 21:40:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for useremail
-- ----------------------------
DROP TABLE IF EXISTS `useremail`;
CREATE TABLE `useremail`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `useremail` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `code` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of useremail
-- ----------------------------
INSERT INTO `useremail` VALUES (1, 'admin', '123456', '1136720013@qq.com', 1, NULL);
INSERT INTO `useremail` VALUES (2, 'admin2', '123456', '377686806@qq.com', 0, NULL);
INSERT INTO `useremail` VALUES (3, 'jiuqi', '123456', '377686806@qq.com', 0, 'f4ff6c5879b74495abb93324a11159403d5fd6680dfd4fc284d24cb76730be4f');

SET FOREIGN_KEY_CHECKS = 1;
